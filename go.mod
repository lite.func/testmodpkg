module example.com/testmodpkg

go 1.14

require (
	github.com/fatih/color v1.9.0 // indirect
	github.com/litefunc/logger v0.0.0-20180902024852-25459d25129f // indirect
	github.com/litefunc/request v0.0.0-20180902025209-0975cf9d0476
	github.com/sirupsen/logrus v1.6.0
)
