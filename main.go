package main

import (
	"example.com/testmodpkg/lib"

	"github.com/litefunc/request"
	"github.com/sirupsen/logrus"
)

func main() {
	logrus.WithFields(logrus.Fields{
		"animal": "walrus",
	}).Info("A walrus appears")
	lib.Print(1)

	lib.Print(request.FormFile{})

	lib.Print(2)
}
